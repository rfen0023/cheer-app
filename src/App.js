import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
        <div id="form-wrapper">
          <form>
            <label>
              Name:
              <input type="text" name="name" />
            </label><br /><br />
            <label>
              Message:
              <input type="text" name="message" id="message-input-box"/>
            </label><br /><br /><br /><br /><br />
            <label>
              Cheers:
              <input type="number" name="cheers" />
            </label><br /><br />
          </form>
          <button id="submit-button">
            Submit
          </button>
        </div>
  );
  }
}

export default App;
