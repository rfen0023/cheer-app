import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

document.getElementById("submit-button").addEventListener("click", function(e) {
    alert('something');
}, false);
